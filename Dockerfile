FROM ubuntu:latest

RUN mkdir /app

WORKDIR /app

COPY entrypoint.sh /

RUN chmod +x /entrypoint.sh

# ENTRYPOINT se ejecuta antes que CMD

ENTRYPOINT ["/entrypoint.sh"]

CMD ["apache2ctl", "-D", "FOREGROUND"]

