## Bootstrap template container

### Modo de uso

Clonar repositorio.

```
$ git clone https://gitlab.com/sergio.pernas2/bootstrap-example.git

$ cd bootstrap-example
```

#### Crear imagen

```
$ sudo docker build -t image-name .
```

#### Lanzar contenedor


```
$ sudo docker run -d --name laboratorio -p 9090:80 -e SITENAME="nombre del sitio" image-name
```
