#!/bin/bash

# Instalacion de paquetes


apt update && apt install apache2 git -y


# Clonar el repositorio

git clone https://gitlab.com/sergio.pernas2/bootstrap-example.git /app


# copiar contenido a directorio /var/www

rm -r /var/www/html

cp -r /app/web /var/www/html


sed -i "s/Cover your page./$SITENAME/" /var/www/html/index.html

# Esta linea ejecuta todo lo que este declarado en la intruccion CMD del fichero Dockefile
exec "$@"
